var fullURL = chrome.extension.getURL("data/json/data.json");

fetch(fullURL)
        .then(res => res.json())
        .then((data) => {

    chrome.storage.local.set({
        'JSON_DATA_URL': data["JSON_DATA_URL"] 
    }, function () {

        chrome.storage.local.get('JSON_DATA_URL', function (obj) {
            fetch(obj['JSON_DATA_URL'])
            .then(res => res.json())
            .then((data) => {        

            chrome.storage.local.set({
                'version': data["version"]
            }, function () {});

            chrome.storage.local.set({
                'getElementsByTagName': data["getElementsByTagName"]
            }, function () {});
            
            chrome.storage.local.set({
                'getElementsByClassName': data["getElementsByClassName"]
            }, function () {});
                    
            chrome.storage.local.set({
                'getElementById': data["getElementById"]
            }, function () {});

            chrome.storage.local.set({
                'querySelector': data["querySelector"]
            }, function () {});
            
            }).catch(err => console.error(err));
        });
    });

}).catch(err => console.error(err));

chrome.storage.local.set({
    'autoplay': 'start'
}, function () {});
