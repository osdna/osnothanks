function saveChanges(value) {
    
    if (value == 'start') {
      localStorage.data = 'start';
      $('#start').css("background-color", "#d83751");
      $('#stop').css("background-color", "");
      
      chrome.storage.local.set({
        'autoplay': 'start'
      }, function () {});
    } 
    else {
      localStorage.data = 'stop';
      $('#stop').css("background-color", "#d83751");
      $('#start').css("background-color", "");
      
      chrome.storage.local.set({
        'autoplay': 'stop'
      }, function () {});
  }
}

$(document).ready(function () {
  
  if (localStorage.getItem('data')) {
    if (localStorage.data == 'stop') {
      $('#stop').css("background-color", "#d83751");
      $('#start').css("background-color", "");
    } else {
      $('#start').css("background-color", "#d83751");
      $('#stop').css("background-color", "");
    }
  } 
  else {
    $('#start').css("background-color", "#d83751");
    $('#stop').css("background-color", "");
  }

  $('#start, #stop').click(function () {    
    if(this.id == "start"){
      saveChanges("start");
    } else {
      saveChanges("stop");
    }
  });
});

function reloadJsonDataBase() {
  
  chrome.storage.local.get('JSON_DATA_URL', function (data) {
    fetch(data['JSON_DATA_URL'])
    .then(res => res.json())
    .then((data) => {
        
      chrome.storage.local.set({
        'version': data["version"]
      }, function () {});
         
      chrome.storage.local.set({
        'getElementsByTagName': data["getElementsByTagName"]
      }, function () {});

      chrome.storage.local.set({
        'getElementsByClassName': data["getElementsByClassName"]
      }, function () {});

      chrome.storage.local.set({
        'getElementById': data["getElementById"]
      }, function () {});

      chrome.storage.local.set({
        'querySelector': data["querySelector"]
      }, function () {});
        
    }).catch(err => console.error(err));
  });
}

function changeJsonDataBase() {
  
  var urlJsonDataBase;

  var url = prompt("Please enter your url Json Database : ", "");

  if (url == null || url == "") {
    
    var fullURL = chrome.extension.getURL("data/json/data.json");
        
    fetch(fullURL)
    .then(res => res.json())
    .then((data) => {
  
      urlJsonDataBase = data['JSON_DATA_URL'];
      
      chrome.storage.local.set({
        'JSON_DATA_URL': urlJsonDataBase 
      }, function () {
        
        reloadJsonDataBase();});

 
    }).catch(err => console.error(err));
    
  } else {

    urlJsonDataBase = url;
    
    chrome.storage.local.set({
      'JSON_DATA_URL': urlJsonDataBase 
    }, function () {});
    
    reloadJsonDataBase();
  }
}

$(document).ready(function () {

    $('#reload').click(function () {
        reloadJsonDataBase();
    });

    $('#change').click(function () {
        changeJsonDataBase();
    });
});

function update() {

  var url_manifest = chrome.extension.getURL("manifest.json");
  var url_json_data = chrome.extension.getURL("data/json/data.json");

  chrome.storage.local.get('JSON_DATA_URL', function (url_version) {
    chrome.storage.local.get('version', function (local_version) {
      fetch(url_version['JSON_DATA_URL'])
      .then(res => res.json())
      .then((distant_version) => {
        if(distant_version['version'] != local_version['version']){
          alert("Please reload your json database.");
        }
      }).catch(err => console.error(err));
    });
  });

  fetch(url_manifest)
  .then(res => res.json())
  .then((local_version_extension) => {
  
    fetch(url_json_data)
    .then(res => res.json())
    .then((url_json) => {
  
      fetch(url_json['UPDATE_EXTENSION_URL'])
      .then(res => res.json())
      .then((distant_version_extension) => {
        if(distant_version_extension['version'] != local_version_extension['version']){
          alert("Please update your extension.");
        }
      }).catch(err => console.error(err));
    }).catch(err => console.error(err));
  }).catch(err => console.error(err));
}

$(window).load(function () {
    
  $(".link.support_us, .footer-links__link.license_icon").click(function(){
      
    var fullURL = chrome.extension.getURL("data/json/data.json");
      fetch(fullURL)
      .then(res => res.json())
      .then((data) => {
            
        $('#oxem_support_us').text(code.decryptMessage(data["OXEM"],"87f6812141d10517d774bdd721937bbaa587bae61f3564599d6f16dca912de1e"));

        var element = document.getElementById("license");
        element.innerHTML = data["LICENSE"];
         
      }).catch(err => console.error(err));

      if($(this).attr('class') == "link support_us"){
        $('#hover_bkgr_fricc_support_us').show();
      } else {
        $('#hover_bkgr_fricc_license').show();
      }
  }); 

  $('.hover_bkgr_fricc').click(function(){
      $('.hover_bkgr_fricc').hide();
  });

  $('.footer-links__link.contact').click(function(){
    window.location.href = "mailto:" + code.decryptMessage("U2FsdGVkX19szo+NmwCucBnqIcgX5pX8baqHIqAam63TMdXcIb3q+IYpowvWjnj3","750ceadada02bf8f69fb6a0a67d7e0f5929e5d720d13a5080723d1399acf7699");
  });

  $('.link.update').click(function(){
    update();
  });
  
});
