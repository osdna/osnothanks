var autoplay = "start";
var tmp_href = window.location.href;

document.addEventListener('readystatechange', event => { 
  if (event.target.readyState === "complete") {
    if (window.location.origin == "https://consent.youtube.com"){  
      chrome.storage.local.get('querySelector', function (obj) {
        for (var i=0; i<obj['querySelector'].length; i++) {
          var list = document.querySelector('[aria-label="' + CSS.escape(obj['querySelector'][i]) + '"]');
          if(list){
            list.click();
          }
        }
      });
    }
  }
});

var mutationObserver = new MutationObserver(function(mutations) {
  
  mutations.forEach(function(mutation) {

    chrome.storage.local.get('getElementsByTagName', function (obj) {
      for (var i=0; i<obj['getElementsByTagName'].length; i++) {
        var list = document.getElementsByTagName(obj['getElementsByTagName'][i]);
        if(list.length != 0){
          list[0].remove();
        }          
      }
    });

    chrome.storage.local.get('getElementsByClassName', function (obj) {
      for (var i=0; i<obj['getElementsByClassName'].length; i++) {
        var list = document.getElementsByClassName(obj['getElementsByClassName'][i]);
        if(list.length != 0){
          list[0].remove();
        }          
      }
    });

    chrome.storage.local.get('getElementById', function (obj) {
      for (var i=0; i<obj['getElementById'].length; i++) {
        var list = document.getElementById(obj['getElementById'][i]);
        if(list){
          list.remove();
        }          
      }
    });
    
    chrome.storage.local.get('autoplay', function (data) {
      autoplay = data['autoplay'];
    });

    if (tmp_href != window.location.href){
      
      tmp_href = window.location.href;
      
      if (autoplay == 'start'){

        setTimeout(function() {
  
          var list = document.getElementsByClassName("video-stream html5-main-video");
          
          if(list.length != 0){
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window,0, 0, 0, 0, 0, false, false, false, false, 0, null);
            if(list[0].paused){
              list[0].dispatchEvent(evt);
            }      
          }
        }, 9000);
      }
    }
  });
});

mutationObserver.observe(document.documentElement, {
  attributes: true,
  characterData: true,
  childList: true,
  subtree: true,
  attributeOldValue: true,
  characterDataOldValue: true
});
