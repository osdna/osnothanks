document.addEventListener("readystatechange", function(evt) {
  if (document.readyState == "interactive") {
    
    var elts = document.getElementsByClassName("tooltip");

    for (var i=0; i<elts.length; i++) {
      elts[i].dataset.bulleId = "";
      elts[i].dataset.title = elts[i].getAttribute("title");
      elts[i].setAttribute("title", "");
      elts[i].addEventListener("mouseenter", showBulle);
      elts[i].addEventListener("mouseleave", hideBulle);
    }
  }
});
  
function showBulle(evt) {
  
  var elt = evt.target;
  
  if (elt.dataset.bulleId != "") {
    return false;
  }
  
  var bulle = document.createElement("div");
  var id = "IB"+new Date().getTime();
  
  bulle.setAttribute("id", id);
  bulle.className = "tooltipJS tooltipAnimateIn";
  bulle.innerHTML = elt.dataset.title;
 
  elt.dataset.bulleId = id;
  elt.appendChild(bulle);
}

function isDescendant(parent, child) {
  var node = child.parentNode;
  while (node != null) {
      if (node == parent) {
          return true;
      }
      node = node.parentNode;
  }
  return false;
}

function hideBulle(evt) {

  var bulle = document.getElementById(evt.target.dataset.bulleId);
  var elt = evt.target;
  
  if(bulle) {
    bulle.className = "tooltipJS tooltipAnimateIn";
  }
  
  setTimeout(function() {
    
    if(bulle != null && isDescendant(elt, bulle)){
      elt.removeChild(bulle);
      elt.dataset.bulleId = "";
    }

  }, 400);
}  
