# osnothanks



The [browser extension](https://chrome.google.com/webstore) named No Thanks is an easy-to-use extension for blocking [YouTube](https://www.youtube.com/) consent pop-ups.

The extension works only on the following website :

- [youtube.com](https://www.youtube.com/) 

  


## Requirements

The extension does not require any dependencies to be installed beforehand in order to ensure its operation.

It is necessary to download beforehand a browser compatible with the chromium extensions as for example the brave browser.


*  [Brave](https://brave.com/)




## Standard installation


To install the latest development version, the following procedure is necessary :

```Text

1. Download the latest version of the extension.

2. Unzip the obtained extension version.

3. Go to the browser extension section.

4. Click to load the unpackaged extension.

5. Select the folder containing the application and click validated.

Latest version : https://gitlab.com/osxcode/osnothanks

```



## Use


Once installed, no user action is required to run the application.

--

However, the user can choose to automatically start playing the video once the link is opened.


![alt text](md_data/img/osnothanks.jpg)


--

The user of the extension can quickly check if his Json database or the extension itself, containing the information needed to block pop-ups is up to date.

To do so, just has to click on the update button. Depending on the URL specified to retrieve the Json data, connection information may be transmitted during the https connection.

The user can at any time specify a different URL for retrieving the Json database used in the extension.
To do this, simply click on the change button to update the new URL.



## Json database structure



Here is an example of Json structure needed for the application to work.
The version key allows to check the consistency of the database used in the extension. 


```json
{
	"version": "0.0.0",
	"getElementsByTagName": [
		"paper-dialog",
		"ytd-popup-container"
	],
	"getElementsByClassName": [
		"gTMtLb fp-nh"
	],
	"getElementById": [
		"dialog"
	]
}
```



The other keys provided and their values allow the removal of pop-ups present on the user's interface.
It is important to note that the YouTube interface can evolve quite quickly, which means that the user must update it.



## Tests



#### Hardware / Software



<span style="color:red">Information - The tests as well as the development of the extension have been carried out with the versions of the following tools and systems: </span>

  

- Brave Version 1.21.76 Chromium: 89.0.4389.86 (Build officiel) (x86_64)

- System  : macOS 10.15.7 (19H114)

  


## Maintainers


Current maintainer :

* osxcode_ - [https://gitlab.com/osxcode/osnothanks](https://gitlab.com/osxcode/osnothanks)



## Privacy

The No Thanks extension does not retrieve or store any personal data of the user during its use.
The extension communicates with a [GitLab](https://about.gitlab.com/privacy/) server in order to retrieve information necessary for its operation. 
During the exchange between the server and the extension, different personal information can be transmitted such as the user's IP address.
The personal information is then subject to [GitLab](https://about.gitlab.com/privacy/)'s privacy policy.


<span style="color:red">The extension does not collect or store any personal data about the user. </span>




## Warning

It is the end user's responsibility to obey all applicable local, state and federal laws.

The author is in no way responsible for any misuse or damage caused by this program.

  

## Support

If you want you can support me ;)

[-- OXEN --](https://oxen.io)


<img src="https://osxcode.gitlab.io/os/img/QRCODE_OXEN_WALLET.png"  width="500" height="350">


## Licence  

[LGPL - Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html)
